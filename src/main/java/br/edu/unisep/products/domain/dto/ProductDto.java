package br.edu.unisep.products.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProductDto {

    private Integer id;
    private String name;
    private String brand;
    private Double price;

}
