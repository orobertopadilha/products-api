package br.edu.unisep.products.domain.usecase;

import br.edu.unisep.products.domain.dto.ProductDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FindAllProductsUseCase {

    public List<ProductDto> execute() {
        var products = new ArrayList<ProductDto>();
        for (int i = 1; i <= 10; i++) {
            products.add(new ProductDto(i, "Product " + i, "Brand " + i, 100.01 + i));
        }

        return products;
    }
}
