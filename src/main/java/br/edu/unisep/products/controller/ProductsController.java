package br.edu.unisep.products.controller;

import br.edu.unisep.products.domain.dto.ProductDto;
import br.edu.unisep.products.domain.usecase.FindAllProductsUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/products")
public class ProductsController {

    private final FindAllProductsUseCase findAllProductsUseCase;

    @GetMapping
    public ResponseEntity<List<ProductDto>> findAll() {
        var products = findAllProductsUseCase.execute();
        return ResponseEntity.ok(products);
    }

}
